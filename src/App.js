import React, { useState } from 'react'
import { onAuthStateChanged } from 'firebase/auth'
import OldTodo from './components/OldTodo';
import Header from '../src/components/header';
import Footer from '../src/components/footer';
import Authentication from "./Services/auth.services"
import { Navigate, Route, Routes } from 'react-router-dom';
import Todo from './components/Todo';
import Login from './Auth/login';
import { auth } from './Services/firebase';
import Signup from './Auth/signup';


function App() {
    const [user, setUser] = useState(null)

     onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
    });

    const RequireLogin = ({ children }) => {
        if (user)
        return (<>{children}</>)
        return <Navigate to="/login" />
        // return user? children : <Navigate to="/login" />
    }
    return (
        <div>
            <Header />
            <Routes>
                <Route path='/' element={<Todo />}></Route>
                <Route path='/old' element={<OldTodo />}></Route>
                <Route path='/login' element={<Login />}></Route>
                <Route path='/signup' element={<Signup />}></Route>
            </Routes>
            <Footer />
        </div>
    )
}

export default App;