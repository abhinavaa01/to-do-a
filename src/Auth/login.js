import { useState } from "react"
import {
    signInWithEmailAndPassword,
    GoogleAuthProvider,
    signInWithRedirect,
    onAuthStateChanged
} from 'firebase/auth'
import { auth, db } from '../Services/firebase'
import { Link, Navigate, useNavigate } from "react-router-dom";

function Login() {
    const navigate = useNavigate();
    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(false)

    onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
    });

    const [values, setValues] = useState({
        email: '',
        password: '',
        errormsg: '',
        successmsg: ''
    })

    const success = (message) => {
        setValues({
            ...values,
            errormsg: '',
            successmsg: message
        })
    }

    const failure = (message) => {
        setValues({
            ...values,
            successmsg: '',
            errormsg: message
        })
    }

    const emailHandler = (e) => {
        setValues({
            ...values,
            email: e.target.value
        })
    }

    const passHandler = (e) => {
        setValues({
            ...values,
            password: e.target.value
        })
    }

    const Signinwithgoogle = async () => {
        setLoading(()=>true);
        // console.log(auth);
            const provider = new GoogleAuthProvider();
            return signInWithRedirect(auth, provider).then((user) => {
                setUser(user);
                navigate(-1);
            }).catch((error)=> {
                failure(error.message);
                setLoading(()=>false);
            });
    }
    // Logging In
    const login = async () => {
        setLoading(()=>true);
            signInWithEmailAndPassword(auth, values.email, values.password).then((user) => {
                navigate(-1);
            }).catch((error)=> {
                failure(error.message);
                setLoading(()=>false);
            })
    }

    if (user)
        return (<Navigate to="/" />)

    return (
        <div className="form-display vh-100 d-flex justify-content-center align-items-center bg-info">
            <div className='my-2 border col-6 mx-auto bg-light rounded-8 shadow'>
                <h2 className='mt-5 text-center'>LOGIN</h2>
                <h5 className='offset-1'>Email</h5>
                <input type='email' autoComplete='email' className='col-10 mb-2 form-control w-80 offset-1' onChange={emailHandler} value={values.email} placeholder='Enter Email...' autoFocus></input><br></br>

                <h5 className='offset-1'>Password</h5>
                <input type='password' autoComplete='password' className='col-10 form-control w-80 offset-1' onChange={passHandler} value={values.password} placeholder='Enter Password...'></input>


                {values.errormsg ? (<div className="form-group text-start mx-3 p-3">
                    <div className="form-check-label alert alert-danger text-capitalized mb-0" role="alert">
                        <i className="bi bi-exclamation-circle-fill"></i> {values.errormsg}
                    </div>
                </div>) : null}
                {values.successmsg ? (<div className="form-group text-start mx-3 p-3">
                    <div className="form-check-label alert alert-success text-capitalized mb-0" role="alert">
                        <i className="bi bi-check-circle-fill"></i> {values.successmsg}
                    </div>
                </div>) : null}

                <button onClick={login} className='btn btn-info col-10 mt-4 offset-1 text-light' disabled={loading}>
                    <span className={loading ? "spinner-border spinner-border-sm" : ""}></span>
                    <span className="ps-2">{loading ? " Loading..." : "login"}</span>
                </button>
                <div id='erroronlogin' className='text-danger mt-2 text-center'></div>
                <div className='row mx-3'>
                    <a className='col'>
                        Forgot Password?
                    </a>
                    <Link className='col ms-auto text-end' to="/signup">
                        New User?
                    </Link>
                </div>
                <div className='ms-auto me-auto my-3 d-flex'>
                    <button type="button" onClick={Signinwithgoogle} class="col-6 btn btn-info mx-auto" >
                        <i className="bi bi-google pe-2"></i> Sign in with Google
                    </button>
                </div>
            </div>
        </div>
    )
}

export default Login