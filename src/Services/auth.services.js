import React,{ useState, useEffect } from 'react'
import {
    sendSignInLinkToEmail,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    onAuthStateChanged,
    sendEmailVerification,
    sendPasswordResetEmail,
    GoogleAuthProvider,
    signInWithRedirect,
} from 'firebase/auth'
import '../components/style/Todo.css'
import { auth, db } from './firebase'
import { ref, set, onValue } from 'firebase/database'
const dbRef = ref(db);


let todotitlelist = JSON.parse(localStorage.getItem('liststodo'));
let createdatelist = JSON.parse(localStorage.getItem('Createdatelists'));
let duedatelist = JSON.parse(localStorage.getItem('Duedatelists'));


function Authentication(props) {

    // const [data, setData] = useState();

    const showlogin = () => {
        document.getElementById('loginform').style.display = 'block';
        document.getElementById('signupform').style.display = 'none';
    }

    const showsignup = () => {
        document.getElementById('signupform').style.display = 'block';
        document.getElementById('loginform').style.display = 'none';
    }

    const actionCodeSettings = {
        // URL you want to redirect back to. The domain (www.example.com) for this
        // URL must be in the authorized domains list in the Firebase Console.
        url: 'https://todoaa0.netlify.app/',
        // This must be true.
        handleCodeInApp: true
    };


    const [registerEmail, setRegisterEmail] = useState("");
    const [registerPass, setRegisterPass] = useState("");

    const [loginEmail, setLoginEmail] = useState("");
    const [loginPass, setLoginPass] = useState("");


    const [user, setUser] = useState({});
    


    // Saving User details on change
    onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
    });



    // Handling the header Login btns
    if (auth.currentUser) {
        document.getElementById('hdrloginbtn').style.display = 'none';
        document.getElementById('hdrsignupbtn').style.display = 'none';
        document.getElementById('hdrlogoutbtn').style.display = 'block';
        document.getElementById('loginform').style.display = 'none';
        document.getElementById('signupform').style.display = 'none';
        const currentloggeduser = user.email;
        document.getElementById("userdetail").innerHTML = "Logged in as: " + currentloggeduser;
        pushtasks(currentloggeduser);
        // pulltasks(currentloggeduser);
        // if (localStorage.getItem('liststodofirebase')>localStorage.getItem('liststodo')){
        //     // 
        //     alert('working');
        // }
    } else if (user === null) {
        document.getElementById('hdrloginbtn').style.display = 'block';
        document.getElementById('hdrsignupbtn').style.display = 'block';
        document.getElementById('hdrlogoutbtn').style.display = 'none';
        document.getElementById("userdetail").innerHTML = "Not Logged in";
    }

    function pushtasks(id) {
        try {
            if (id) {

        // setting username from email
        const uid = id.split('.')[0]+id.split('.')[1];
        const username = uid.split('@')[0]+uid.split('@')[1];

        // pushing data into the realtime database
        set(ref(db, 'users/' + username), {
            duedatelist : duedatelist,
            createdatelist: createdatelist,
            todotitlelist: todotitlelist,
          });
        }
        } catch (error) {
            console.log(error);
            alert(error.message);
        }
    }

    // saving file into localstorage after fetching from firebase
    function savingtasks(tododata, filename) {
        localStorage.setItem(filename, JSON.stringify(tododata));
    }

    function pulltasks(id) {
        try {
            if (id) {

        // setting username from email
        const uid = id.split('.')[0]+id.split('.')[1];
        const username = uid.split('@')[0]+uid.split('@')[1];

        // pulling data into the realtime database
        onValue(ref(db, 'users/' + username), (snapshot) => {
            var tododata = (snapshot.val().todotitlelist);
            console.log('tododata:=  '+ tododata);
            var filename = "liststodofirebase";
            savingtasks(tododata, filename);
          });
          onValue(ref(db, 'users/' + username), (snapshot) => {
              var tododata = (snapshot.val().createdatelist);
              var filename = "listscreatedatefirebase";
              savingtasks(tododata, filename);
            });
            onValue(ref(db, 'users/' + username), (snapshot) => {
                var tododata = (snapshot.val().duedatelist);
                var filename = "listsduedatefirebase";
                savingtasks(tododata, filename);
              });
        }
        } catch (error) {
            console.log(error);
            alert(error.message);
        }
    }

    // Signing Up
    const signup = async () => {
        try {
            const user = await createUserWithEmailAndPassword(auth, registerEmail, registerPass);
            setUser(user);
            setRegisterPass("");
            // const idToken = user.IdToken;
            setTimeout(sendEmailVerification(auth.user).then(alert('Verification Sent!')), 3000);
        } catch (error) {
            console.log(error);
            alert(error.message);
        }
    };

    // Logging In
    const login = async () => {
        try {
            const user = await signInWithEmailAndPassword(auth, loginEmail, loginPass);
            setUser(user);
            setLoginPass("");
            // pulltasks();
        } catch (error) {
            document.getElementById('erroronlogin').innerHTML = error.message;
            alert(error.message);
        }
    };

    // Signing in with google
    const Signinwithgoogle = async () => {
        try {
            const provider = new GoogleAuthProvider();
            return signInWithRedirect(auth, provider)
        } catch (error) {
            document.getElementById('erroronsignup').innerHTML = error.message;
        }
    };


    // Forget Password link
    const forgotpass = async () => {
        try {
            sendPasswordResetEmail(auth, loginEmail)
                .then(() => {
                    // Password reset email sent!
                    alert('Reset Link Sent!');
                })
        } catch (error) {
            alert(error.message);
        }
    };




    return (
        <div>
            <div id='signupform' className="form-display border my-5 col-8 offset-2">
                <h2 className='mt-5 text-center'>Signup</h2>


                <div className='my-2'>
                    <h5 className='offset-1'>Email</h5>
                    <input type='email' autoComplete='email' className='col-10 mb-2 form-control w-80 offset-1' onChange={(event) => setRegisterEmail(event.target.value)} placeholder='Enter Email...' autoFocus></input><br></br>

                    <h5 className='offset-1'>Password</h5>
                    <input type='password' autoComplete='password' className='col-10 form-control w-80 offset-1' onChange={(event) => setRegisterPass(event.target.value)} placeholder='Enter Password...'></input>

                    <button onClick={signup} className='btn btn-outline-success col-10 mt-4 offset-1'>Signup</button>
                    <div id='erroronsignup' className='text-danger my-2 text-center'></div>
                    <a onClick={showlogin} className='mx-3 my-4'>
                        Existing User?
                    </a>
                    <div className='ms-auto me-auto my-3'>
                        <button type="button" onClick={Signinwithgoogle} class="signin-with-google-btn offset-md-3 offset-1 col-md-6 col-10" >
                        Sign in with Google
                    </button>
                    </div>
                </div>
            </div>


            <div id='loginform' className="form-display border my-5 col-8 offset-2">
                <h2 className='mt-5 text-center'>Login</h2>


                <div className='my-2'>
                    <h5 className='offset-1'>Email</h5>
                    <input type='email' autoComplete='email' className='col-10 mb-2 form-control w-80 offset-1' onChange={(event) => setLoginEmail(event.target.value)} placeholder='Enter Email...' autoFocus></input><br></br>

                    <h5 className='offset-1'>Password</h5>
                    <input type='password' autoComplete='password' className='col-10 form-control w-80 offset-1' onChange={(event) => setLoginPass(event.target.value)} placeholder='Enter Password...'></input>

                    <button onClick={login} className='btn btn-outline-success col-10 mt-4 offset-1'>Login</button>
                    <div id='erroronlogin' className='text-danger mt-2 text-center'></div>
                    <div className='row mx-3'>
                    <a onClick={forgotpass} className='col'>
                        Forgot Password?
                    </a>
                    <a onClick={showsignup} className='col ms-auto text-end'>
                        New User?
                    </a>
                    </div>
                    <div className='ms-auto me-auto my-3'>
                        <button type="button" onClick={Signinwithgoogle} class="signin-with-google-btn offset-md-3 offset-1 col-md-6 col-10" >
                        Sign in with Google
                    </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Authentication;
