import { db } from '../Services/firebase'
import { child, get, onValue, ref, set } from 'firebase/database'

export function pushTasks(email, data) {
    const username = email?.split("@")[0];
    return set(ref(db, 'todos/' + username), {
        tasks: data
      });
}

export function pullTasks(email) {
    const username = email?.split("@")[0];
    return get(child(db, "todos/" + username))
}

export function pullTaskssecond(email) {
    const username = email?.split("@")[0];
    const status = new Promise((resolve, reject)=> {
    // pulling data into the realtime database
    onValue(ref(db, 'todos/' + username), (snapshot) => {
        const data = snapshot.val();
        // console.log(data);
        resolve(data)
      });
    })
    return status;
}