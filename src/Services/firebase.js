// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBA34hXHAJJIKnHKyc7L8zqz-mLdyDBZHE",
  authDomain: "todo-app-3a405.firebaseapp.com",
  projectId: "todo-app-3a405",
  storageBucket: "todo-app-3a405.appspot.com",
  messagingSenderId: "459979017277",
  appId: "1:459979017277:web:8eaec950de2d630136ae54",
  measurementId: "G-WSBC3GMQT0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
//const analytics = getAnalytics(app);
export const auth = getAuth(app);