import React, { useEffect } from 'react';
import './style/ShowTodo.css';


function NewShowTodo(props) {
    const data = props.task;
    const index = props.index;
    useEffect(() => {
        // console.log(typeof data.createdate.toLocaleDateString());
    }, [data])
    return (
        <div className='px-2 my-2 shadow-sm rounded-3 py-1 shadow-3-strong' id={data.id}>
            <div className="row">

                <div className="col-6 col-md-9 pt-2 text-truncate">
                    <h4>{data.task}</h4>
                    <div className={data.done ? "text-success" : "text-danger"}>Done: {data.done ? "Yes" : "No"}</div>
                </div>
                {/*  */}
                <div className="dropstart me-2 w-auto ms-auto">
                    <a className="text-black btn btn-info" type="button" data-bs-toggle="dropdown" aria-expanded="false" title="More options">
                        <i className="bi bi-three-dots-vertical"></i>
                    </a>
                    <ul className="dropdown-menu row">
                        <li className="dropdown-item">
                            <button className='btn btn-info w-100' title={data.done?'Undo':'Done'} onClick={() => props.markDone(index)}>
                                <i className={data.done?"bi bi-arrow-counterclockwise pe-1 pe-md-2":"bi bi-check-square pe-1 pe-md-2"}></i>{data.done?'Undo':'Done'}
                            </button>
                        </li>
                        <li className="dropdown-item">
                            <button className='btn btn-info w-100' title='Edit' onClick={() => props.editit(data, index)}>
                                <i className="bi bi-pencil-square pe-1 pe-md-2"></i>Edit
                            </button>
                        </li>
                        <li className="dropdown-item">
                            <button className='btn btn-info w-100' title='Delete' onClick={() => props.deleteit(index)} >
                                <i className="bi bi-trash pe-1 pe-md-2"></i>Delete
                            </button>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="row">
                <div className="dateclasses col-6 pt-1">
                    Date Created :
                </div>
                <div className="dateclasses col-6 pt-1">
                    Due date :
                </div>
            </div>

            <div className="row">
                <div className="dateclasses col-6 text-truncate">
                    {data.createdate}
                </div>
                <div className="dateclasses col-6 text-truncate">
                    {data.duedate}
                </div>
            </div>

        </div>
    )
}


export default NewShowTodo;