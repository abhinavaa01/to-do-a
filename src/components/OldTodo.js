import React, { useEffect, useState } from 'react'
import ShowTodo from './ShowTodo'
import DatePicker from 'react-date-picker'
import dateFormat from "dateformat";
import './style/Todo.css'


//for dateformat reference
// dateFormat("2022-06-05T03:57:50.187Z", "dddd, mmmm dS, yyyy");


let Createdatelist = JSON.parse(localStorage.getItem('Createdatelists'));
let Duedatelist = JSON.parse(localStorage.getItem('Duedatelists'));

const getLocalItem = () => {
    let list = localStorage.getItem('liststodo');
    if (list) {
        return JSON.parse(list)
    } else {
        return [];
    }
}

const getLocalItemCDate = () => {
    if (Createdatelist) {
        return Createdatelist
    } else {
        return [];
    }
}

const getLocalItemDDate = () => {
    if (Duedatelist) {
        return Duedatelist
    } else {
        return [];
    }
}

function OldTodo() {

    const [task, setTask] = useState();
    const [data, setData] = useState(getLocalItem());

    const [createdate, setCreatedate] = useState(new Date());
    const [createdatedata, setCreatedateData] = useState(getLocalItemCDate());

    const [duedate, setDuedate] = useState(new Date());
    const [duedatedata, setDuedateData] = useState(getLocalItemDDate());

    const onChangeHandler = (e) => {
        setTask(e.target.value)
    }

    const submitHandler = (e) => {
        e.preventDefault();

        if (document.getElementById("todo-input").value === "") {
            window.alert('Please enter Task name');
        } else if (createdate.getTime() > duedate.getTime()) {
            window.alert("Start date cnnot be after end date")
        }
        else {
            const newCreatedateData = dateFormat(createdate, "dddd, mmmm dS, yyyy");
            const newDuedateData = dateFormat(duedate, "dddd, mmmm dS, yyyy");

            const newData = task;
            setData([...data, newData]);
            setCreatedateData([...createdatedata, newCreatedateData]);
            setDuedateData([...duedatedata, newDuedateData]);
            window.location.reload();
        }

    }

    const deleteItem = (a) => {
        const finalData = data.filter((curEle, index) => {
            return index !== a;
        })
        setData(finalData);

        const finalCreatedateData = createdatedata.filter((curEle, index) => {
            return index !== a;
        })
        setCreatedateData(finalCreatedateData);

        const finalduedateData = duedatedata.filter((curEle, index) => {
            return index !== a;
        })
        setDuedateData(finalduedateData);
    }


    const editItem = (a) => {


        // Changing css for edit function
        document.getElementById(a).classList.toggle('bg-danger');
        document.getElementById(a).classList.toggle('bg-opacity-25');

        document.getElementById(a + 'editbtn').classList.toggle('bi-check2-all');
        document.getElementById(a + 'editbtn').classList.toggle('bi-pencil-square');


        // eslint-disable-next-line
        const finalData1 = data.filter((curEle, index) => {
            return index = a;
        })
if (document.getElementById(a + 'editbtn').classList.contains('bi-pencil-square')) {
            document.getElementById(a + 'taskname').contentEditable = 'false';
            document.getElementById(a + 'taskstartdate').contentEditable = 'false';
            document.getElementById(a + 'taskduedate').contentEditable = 'false';
            // setData([...data, document.getElementById(a + 'taskname').innerText]);
            // setCreatedateData([...createdatedata, document.getElementById(a + 'taskstartdate').innerHTML]);
            // setDuedateData([...duedatedata, document.getElementById(a + 'taskduedate').innerHTML]);

            var editname = [...data];
            editname[a] = document.getElementById(a + 'taskname').innerText;
            setData(editname);

            var editcreatedate = [...createdatedata];
            editcreatedate[a] = document.getElementById(a + 'taskstartdate').innerText;
            setCreatedateData(editcreatedate);

            var editduedate = [...duedatedata];
            editduedate[a] = document.getElementById(a + 'taskduedate').innerText;
            setDuedateData(editduedate);


        }
else {
            document.getElementById(a + 'taskname').contentEditable = 'true';
            document.getElementById(a + 'taskstartdate').contentEditable = 'true';
            document.getElementById(a + 'taskduedate').contentEditable = 'true';
        }
    }

    useEffect(() => {
        localStorage.setItem("liststodo", JSON.stringify(data));
        localStorage.setItem('Createdatelists', JSON.stringify(createdatedata));
        localStorage.setItem('Duedatelists', JSON.stringify(duedatedata));
    })

    return (
        <div className="container">
            <div className="row mx-auto main-row">
                <div className="col shadow main-col">
                    <div className="row bg-success bg-opacity-75 text-white">
                        <div className="row  p-2">
                            <i className="bi bi-journals h4 col-2"></i>
                            <h4 className='h4 text-center'>Todo List</h4>
                        </div>
                    </div>
                    <form id='forminput' className='bg-info bg-opacity-25 border mt-2' onSubmit={submitHandler}>
                        <div className="row justify-content-between text-white p-2">
                            <div className="form-group flex-fill mb-2 col-9">
                                <input id="todo-input" autoFocus="autofocus" type="text" className="form-control" placeholder="Add a To-do" value={task} onChange={onChangeHandler} />
                            </div>
                            <button type="submit" id="addtodo" className="bg-success bg-opacity-75 btn text-light mb-2 ml-1 col-3 add-btn"><i className="bi bi-plus-square"></i> Add</button>
                        </div>


                        <div className="row justify-content-between text-dark">
                            <div className='col-6'>Task Started on :</div>
                            <div className='col-6'>Task Due on :</div>
                        </div>

                        <div className="row justify-content-between">
                            <DatePicker onChange={setCreatedate} value={createdate} className="col-6 mx-auto" format='dd-MM-y' />
                            <DatePicker onChange={setDuedate} value={duedate} className="col-6 mx-auto" format='dd-MM-y' />
                        </div>
                    </form>



                    {data.map((value, index) => {
                        return <div><ShowTodo
                            key={index}
                            id={index}
                            task={value}
                            createdate={Createdatelist}
                            duedate={Duedatelist}
                            onSelcet={deleteItem}
                            onSelect={editItem}
                        />
                        </div>
                    })}


                </div>
            </div>
        </div>
    )
}

export default OldTodo