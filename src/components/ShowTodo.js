import React from 'react';
import './style/ShowTodo.css';


function ShowTodo(props) {
    return (
        <div id={props.id} className='container my-2 border pb-1 pt-1'>
        <div className="row">

        <div className="col-8 pt-2 text-truncate">
                <h4 id={props.id + 'taskname'}>{props.task}</h4>
            </div>
            <div className="col-1 my-auto">
            <button className='rounded-circle btn btn-outline-info' title='Edit' id={props.id+'editt'} onClick={()=>{
                props.onSelect(props.id)
            }}><i id={props.id + 'editbtn'} className="bi bi-pencil-square"></i></button>
            </div>
            <div className="offset-1 col-1 my-auto">
            <button className='rounded-circle btn btn-outline-danger' title='Delete' onClick={()=>{
                props.onSelcet(props.id)
            }}><i className="bi bi-trash"></i></button>
            </div>
        </div>

        <div className="row">
            <div className="dateclasses col-6 pt-1">
                    Date Created :
                </div>
            <div className="dateclasses col-6 pt-1">
                    Due date :
                </div>
        </div>

        <div className="row">
            <div id={props.id + 'taskstartdate'} className="dateclasses col-6 text-truncate">
                {props.createdate[props.id]}
            </div>
            <div id={props.id + 'taskduedate'} className="dateclasses col-6 text-truncate">
                {props.duedate[props.id]}
            </div>
        </div>
            
        </div>
    )
}


export default ShowTodo;