import React, { useEffect, useState } from 'react'
import DatePicker from 'react-date-picker'
import './style/Todo.css'
import NewShowTodo from './NewShowTodo';
import { onAuthStateChanged } from '@firebase/auth';
import { auth } from '../Services/firebase';
import { pullTasks, pullTaskssecond, pushTasks } from '../Services/data.services';
import { Navigate } from 'react-router';


function Todo() {
    const [startDateInput, setStartDateInput] = useState(new Date());
    const [dueDateInput, setDueDateInput] = useState(new Date());
    const [dataList, setDataList] = useState([]);
    const [editingTask, setEditingTask] = useState(() => false);
    const [sync, setSync] = useState(() => false);
    const [savingTask, setSavingTask] = useState({
        status: "",
        message: ""
    });
    const [theTodo, setTheTodo] = useState({
        task: '',
        createdate: '',
        duedate: '',
        done: false,
        id: Date.now()
    });
    const [user, setUser] = useState(null)

    onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
    });

    useEffect(() => {
        let list = JSON.parse(localStorage.getItem("Todos"))
        if (list)
            setDataList(list)
        // console.log(list);
    }, [])

    useEffect(() => {
        setTheTodo({
            ...theTodo,
            createdate: startDateInput.toLocaleDateString(),
            duedate: dueDateInput.toLocaleDateString()
        })
        if (savingTask.status) {
            setSavingTask({
                status: "",
                message: ""
            });
        }
    }, [startDateInput, dueDateInput])

    useEffect(() => {
        // console.log(dataList);
        if (dataList?.length) {
            localStorage.setItem("Todos", JSON.stringify(dataList))
            if (user && dataList && sync) {
                pushData();
            }
        }
    }, [dataList])

    const taskChangeHandler = (e) => {
        setTheTodo({
            ...theTodo,
            task: e.target.value
        });
        if (savingTask.status) {
            setSavingTask({
                status: "",
                message: ""
            });
        }
    }

    const submitHandler = (e) => {
        e.preventDefault();
        setEditingTask(() => false)
        if (theTodo.task) {
            setDataList([...dataList, theTodo])
            setTheTodo({
                ...theTodo,
                task: ''
            })
            setStartDateInput(new Date())
            setDueDateInput(new Date())
        }
    }

    const deleteit = (index) => {
        // console.log(data);
        if (window.confirm("Confirm Delete?") == true) {
            let newArr = dataList.filter((element, place) => place != index);
            setDataList(newArr);
        }
    }

    const editit = (data, index) => {
        setEditingTask(() => true)
        let tempCreate = data.createdate.split("/");
        let tempCreateDate = new Date(tempCreate[2], tempCreate[1] - 1, tempCreate[0]);
        let tempDue = data.createdate.split("/");
        let tempDueDate = new Date(tempDue[2], tempDue[1] - 1, tempDue[0]);
        setStartDateInput(tempCreateDate);
        setDueDateInput(tempDueDate);
        setTheTodo({
            ...theTodo,
            task: data.task
        });
        let newArr = dataList.filter((element, place) => place != index);
        setDataList(newArr);
    }

    const markasDone = (index) => {
        const temp = dataList.map((value, place) => {
            if (place == index) {
                return {
                    ...value,
                    done: !value.done
                };
            } else {
                return value
            }
        })
        setDataList(temp);
    }

    const pullData = (e) => {
        e.preventDefault();
        setSavingTask({
            status: "fetching",
            message: "Fetching data"
        })
        pullTaskssecond(user.email).then((result)=> {
            // console.log(result.tasks);
            setDataList(result.tasks);
            setSavingTask({
                status: "saved",
                message: "Fetched data successfully"
            })
        }).catch((error)=> {
            console.error(error);
        })
    }

    const pushData = () => {
        setSavingTask({
            status: "Saving",
            message: "Saving Tasks"
        })
        pushTasks(user.email, dataList).then((result) => {
            setSavingTask({
                status: "saved",
                message: "Saved Successfully"
            })
        }).catch((error) => error)
    }
    // if (!user) {
    //     return <Navigate to="/login" />
    // }

    return (
        <div className="">
            <div className="row mx-auto main-row">
                <div className="col main-col">
                    <div className="row bg-info text-white">
                        <div className="row justify-content-between p-2">
                            <i className="bi bi-journals h4 w-auto"></i>
                            <h4 className='h4 text-center w-auto'>Todo List</h4>
                            <i className="bi bi-cloud-arrow-down-fill h4 w-auto ms-auto" title='Pull stored data' onClick={pullData}></i>
                            <i className="bi bi-cloud-arrow-up-fill h4 w-auto" title='Push to store data' onClick={pushData}></i>
                            <i class={sync ? "bi bi-cloud-check-fill h4 w-auto" : "bi bi-cloud-slash-fill h4 w-auto"} onClick={() => setSync((sync) => !sync)} title={sync ? "Sync is on!" : "Sync is off!"}></i>
                        </div>
                    </div>
                    <form id='forminput' className=' my-2 p-2' onSubmit={submitHandler}>
                        <div className="row justify-content-between text-white p-2">
                            <div className="form-group flex-fill mb-2 col-9">
                                <input id="todo-input" autoFocus="autofocus" type="text" className="form-control" placeholder="Add a To-do" value={theTodo.task} onChange={taskChangeHandler} />
                            </div>
                            <button type="submit" id="addtodo" className="btn btn-info text-light mb-2 ml-1 col-3 add-btn" disabled={!theTodo.task}>
                                {editingTask ? <span><i className="bi bi-check-square pe-3"></i>Save</span> :
                                    <span><i className="bi bi-plus-square pe-md-3 pe-1"></i>Add</span>
                                }

                            </button>
                        </div>


                        <div className="row justify-content-between text-dark">
                            <div className='col-6'>Task Started on :</div>
                            <div className='col-6'>Task Due on :</div>
                        </div>

                        <div className="row justify-content-between px-2">
                            {/* <input type='date' className='col-6 mx-auto bg-transparent w-auto' onChange={setStartDateInput} value={startDateInput} /> */}
                            <DatePicker onChange={setStartDateInput} value={startDateInput} className="col-6 mx-auto" format='dd-MM-y' />
                            <DatePicker onChange={setDueDateInput} value={dueDateInput} className="col-6 mx-auto" format='dd-MM-y' />
                        </div>

                        {savingTask.status ? (<div className="form-group text-start py-3">
                            <div className="form-check-label alert alert-success text-capitalized mb-0" role="alert">
                                <i className={savingTask.status == "Saving" ? "spinner-border spinner-border-sm mx-2" : "bi bi-check-circle-fill mx-2"}></i>{savingTask.message}
                            </div>
                        </div>) : null}
                    </form>
                    <hr className='text-info py-1 rounded-3' />
                    <div className='hover-overlay'>


                        {dataList?.map((value, index) => {
                            // console.log(index);
                            return <NewShowTodo
                                key={index}
                                index={index}
                                id={value.id}
                                task={value}
                                deleteit={deleteit}
                                editit={editit}
                                markDone={markasDone}
                            />
                        })}

                    </div>
                </div>
            </div>
        </div>
    )
}

export default Todo