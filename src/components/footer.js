import { useState } from "react";
import { onAuthStateChanged } from 'firebase/auth'
import { auth } from "../Services/firebase";

function Footer() {
    const [user, setUser] = useState(null)

     // Saving User details on change
     onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
    });

    return (
        <div className="mt-2 row border fixed-bottom bg-light">
            <p className="my-auto col-4 ms-3 my-2">© Vishal Sharma</p>
            {user?<p className="my-auto col-6 text-end my-2">Logged in as: {user.displayName? user.displayName : "Name not set"}</p>:
            <p className="my-auto col-6 text-end my-2">Not logged in</p>}
        </div>
    )
}

export default Footer;