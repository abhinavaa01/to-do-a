import logo from '../Images/download.png'
import './style/Todo.css'
import { onAuthStateChanged } from 'firebase/auth'
import { auth } from "../Services/firebase";
import { signOut } from 'firebase/auth'
import { Link } from 'react-router-dom';
import { useState } from 'react';

function Header() {
    const [user, setUser] = useState(null)

    // Saving User details on change
    onAuthStateChanged(auth, (currentUser) => {
        setUser(currentUser);
        // console.log(currentUser);
    });

    const logout = async () => {
        await signOut(auth);
        window.location.reload(false);
    };

    return (
        <div className='mb-5'>
            <div className="row border mb-2 bg-white fixed-top px-3">
                <Link to="/" className='header-logo my-auto mb-1 mt-1'>
                    <img src={logo} className="h-100 w-100 my-auto" alt="Todo"></img>
                </Link>
                {user ?
                    <div className='dropdown w-auto ms-auto'>
                        {/* <button onClick={logout} className='btn btn-info ms-auto me-3 w-auto h-50 my-auto'>Logout</button> */}
                        <button className="header-user-btn fs-2 btn btn-default p-0 m-2 dropdown-toggle" type="button" id="menu1" data-bs-toggle="dropdown" aria-expanded="false">
                            {user.photoURL?
                            <img src={user.photoURL} className='header-user-btn my-auto rounded-circle p-0' />:
                            <i className="bi bi-person-circle"></i>}
                        </button>
                        <ul className="dropdown-menu">
                            <li><span className='dropdown-item'>Name: {user.displayName? user.displayName : "Not set"}</span></li>
                            <li><span className='dropdown-item'>Email: {user.email}</span></li>
                            <li><button className="dropdown-item fw-bold" onClick={logout}>Logout</button></li>
                        </ul>
                    </div> :
                    <>
                        <Link to="/login" className='btn btn-info ms-auto w-auto h-50 my-auto'>Login</Link>
                        <Link to="/signup" className='btn btn-info ms-3 me-3 w-auto h-50 my-auto'>Signup</Link>
                    </>}
            </div>
        </div>
    )
}

export default Header;
